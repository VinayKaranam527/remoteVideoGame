package com.video.game.videogame.simulation;

public class Dancer extends Performer {
	
	private String styleOfDance;
	private int id;
	
	public String getStyleOfDance() {
		return styleOfDance;
	}
	public void setStyleOfDance(String styleOfDance) {
		this.styleOfDance = styleOfDance;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Dancer(int id, String styleOfDance)
	{
		super(id);
		this.id = id;
		this.styleOfDance = styleOfDance;
	}
	public String announce() {
		// TODO Auto-generated method stub
		{
			String performerId = String.valueOf(id);
			return styleOfDance+ " - "+ performerId + " - dancer";
		}
	

}
}
