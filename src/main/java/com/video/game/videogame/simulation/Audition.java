package com.video.game.videogame.simulation;

import java.util.ArrayList;
import java.util.List;

public class Audition {

	public List<String> createAudition()
	{
		
		List<Performer> performers = new ArrayList<Performer>();
		List<String> list = new ArrayList<String>();
		performers.add(new Performer(123));
		performers.add(new Performer(156));
		performers.add(new Performer(198));
		performers.add(new Performer(202));
		performers.add(new Dancer(124,"tap"));
		performers.add(new Dancer(125,"Zumba"));
		performers.add(new Vocalist(130,"K"));
		
		for(Performer performer: performers)
		{
			list.add(performer.announce());
		}
		return list;
	}
}

