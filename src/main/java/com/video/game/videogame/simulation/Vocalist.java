package com.video.game.videogame.simulation;

public class Vocalist extends Performer {
	
	private String key;
	private int volumeRange;
	private int id;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public int getVolumeRange() {
		return volumeRange;
	}
	public void setVolumeRange(int volumeRange) {
		this.volumeRange = volumeRange;
	}
	
	public Vocalist(int id, String key)
	{
		super(id);
		this.id = id;
		this.key =key;
	}
	public Vocalist(int id,String key, int volumeRange)
	{
		super(id);
		this.id= id;
		this.key =key;
		this.volumeRange = volumeRange;
	}
	
	public String announce() {
		
		String performerId = String.valueOf(id);
		return "I sing in the key of - " + key + " - " +performerId; 
	}
	
	public String announceWithVolume()
	{
		String performerId = String.valueOf(id);
		if(volumeRange <1 || volumeRange >10)
		{
			throw new IllegalArgumentException("Invalid volume range");
		}
		else
		{
		return "I sing in the key of - " + key + " - " +"at the volume "+volumeRange +" - "+performerId;
		}
	}

}
