package com.video.game.videogame.simulation;

import static org.junit.Assert.*;

import java.util.Objects;

import org.junit.Before;
import org.junit.Test;

public class PerformerTest {

    private Performer performer;
	@Before
	public void setup()
	{
		performer = new Performer(123);
	}

	@Test
	public void testGetPerformerId() {
		assertEquals(123,performer.getPerformerId());
			}

   // positive Test case
	@Test
	public void testAnnounce() {
		assertEquals("123 - performer",performer.announce());
	}
	
	//negative Test case
	@Test
	public void testAnnounce2()
	{
		String expMsg = "183 - performer";
		assertFalse(Objects.equals(expMsg, performer.announce()));
	}
	

}
