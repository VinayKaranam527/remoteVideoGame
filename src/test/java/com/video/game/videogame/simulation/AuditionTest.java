package com.video.game.videogame.simulation;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.junit.Before;
import org.junit.Test;

public class AuditionTest {

	private Audition audition;
	@Before
	public void setUp()
	{
		audition = new Audition();
	}
	
	//Positive case
	@Test
	public void testCreateAudition1() {
		
		List<String> explist = Arrays.asList("123 - performer", "156 - performer" , "198 - performer", "202 - performer", "tap - 124 - dancer", "Zumba - 125 - dancer", "I sing in the key of - K - 130");
		assertEquals(explist,audition.createAudition());
	}
	
	//Negative case
	@Test
	public void testCreateAudition2()
	{
		List<String> explist = Arrays.asList("123 - performer", "156 " , "198 - performer", "202 - performer", "tap - 124 - dancer", "Zumba - 125 - dancer", "I sing in the key of - K - 130");
		assertFalse(Objects.equals(explist,audition.createAudition()));
	}

}
