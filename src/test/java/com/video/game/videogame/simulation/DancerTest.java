package com.video.game.videogame.simulation;

import static org.junit.Assert.*;

import java.util.Objects;

import org.junit.Before;
import org.junit.Test;

public class DancerTest {

	private Dancer dancer;
	@Before
	public void setup()
	{
	   dancer = new Dancer(123,"tap");	
	}
	//positive case
	@Test
	public void testAnnounce() {
		
		assertEquals("tap - 123 - dancer",dancer.announce());
	}
	
	//Negative case
	@Test
	public void testAnnounce2() {
		String expMsg = "gear - 123 - dancer";
		assertFalse(Objects.equals(expMsg,dancer.announce()));
	}
	
 //positive case
	@Test
	public void testGetStyleOfDance() {
		assertEquals("tap",dancer.getStyleOfDance());
	}

	@Test
	public void testGetId() {
		assertEquals(123,dancer.getPerformerId());
	}

}
