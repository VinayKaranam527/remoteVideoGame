package com.video.game.videogame.simulation;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class VocalistTest {

	private Vocalist vocalist;
	@Before
	public void setup()
	{
		vocalist = new Vocalist(156,"G");
		
	}

	
	@Test
	public void testAnnounce()
	{
		assertEquals("I sing in the key of - G - 156",vocalist.announce());
	}


	@Test
	public void testGetVolumeRange() {
		vocalist = new Vocalist(123,"K",8);
		assertEquals(8,vocalist.getVolumeRange());
	}
	
	@Test
	public void testGetKey() {
		vocalist = new Vocalist(123,"K",8);
		assertEquals("K",vocalist.getKey());
	}
	
	@Rule public ExpectedException thrown = ExpectedException.none();
	@Test
	public void testAnnounceWithVolume() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid volume range");
		assertEquals("234",new Vocalist(123,"K",12).announceWithVolume());
	}
	

}
